# To-Do App

The Todo App is a simple mobile task list application (To-Do app) that allows users to create, view, edit, and delete tasks. The application is built using Flutter, following the principles of Clean Architecture and utilizing GetX as a state manager.

## Key Features

- **Create Tasks**: Allows users to add new tasks by providing a title.

- **View Tasks**: Users can see a list of all existing tasks, along with their current status.

- **Edit Tasks**: Offers the ability to modify existing tasks, allowing changes to their title.

- **Delete Tasks**: Allows users to remove tasks that are no longer needed.

## Architecture

The application follows the principles of Clean Architecture, dividing the logic into well-defined layers: Presentation, Domain, and Data. This provides a clear separation of concerns and facilitates testing and maintenance of the code.

- **Presentation**: Contains logic related to the user interface (UI), using GetX for state management and navigation between screens.

- **Domain**: Defines the business rules and logic of the application, including data models and use cases.

- **Data**: Handles interaction with external data sources, in this case use the lib  Shared Preferences.

## Usage Instructions

To use the Todo App, simply open the application on your mobile device. From there, you'll be able to create new tasks, view existing tasks, edit tasks, and delete tasks as needed.

## Getting Started

To get started with development, follow these steps:

1. Clone this repository to your local machine:

```bash
git clone <https://gitlab.com/rgarcia16/flutter_todo_app.git>
```

2. Open the project in your preferred code editor.

3. Make sure you have Flutter and Dart installed on your system. If not, follow the instructions at flutter.dev to install Flutter.

4. Get the project dependencies by running the following command in the terminal:

```bash
flutter pub get
```

Once completed, you can compile and run the application on an emulator or connected device with the following command:

```bash
flutter run
```
## Documentation

### Directory Structure

The project follows a typical directory structure for a Flutter application. Below is a brief overview of the directories and their purposes:

- **lib/**: Contains the main source code of the application.
    - **features/**: Contains modules or features of the application, each with its own presentation, data, and domain logic.
      - **common/**: Contains a handler that injects the common project dependencies
      - **task/**: Contains entities, use cases, repositories, or other components related to tasks, which are shared across multiple layers of the Clean Architecture.
          - **data/**: Handles interaction with external data sources, such as databases or APIs.
          - **domain/**: Defines the business logic and rules of the application.
            - **presentation/**: Contains the user interface (UI) logic and components, including screens, widgets, and state management.
    - **config/**: Contains application-specific configuration files, such as the routes and theme.
    - **shared/**: Contains code shared between different parts of the application, in this case the is configured shared preferences lib.


- **test/**: Contains unit tests for the application code.

- **android/**: Contains Android-specific configuration files and resources.

- **ios/**: Contains iOS-specific configuration files and resources.

- **assets/**: Contains static assets used by the application, such as images, fonts, or localization files.

**Nota**
Each controller has a separate binding file where sets up the dependencies required for the controller.
