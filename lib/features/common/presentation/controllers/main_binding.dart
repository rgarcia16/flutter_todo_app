import 'package:get/get.dart';
import 'package:todo_app/features/tasks/data/datasources/local_task_datasource_impl.dart';
import 'package:todo_app/features/tasks/data/repositories/task_repository_impl.dart';
import 'package:todo_app/features/tasks/domain/datasources/task_datasource.dart';
import 'package:todo_app/features/tasks/domain/repositories/task_repository.dart';

/// Binding class for initializing dependencies in the main application.
class MainBinding extends Bindings {
  /// Initializes dependencies required for the application.
  @override
  void dependencies() {
    // Create local data source and API data source instances
    final localDataSource =
        Get.put<TaskDataSource>(LocalTaskDataSourceImpl(), tag: "local");
    final apiDataSource =
        Get.put<TaskDataSource>(LocalTaskDataSourceImpl(), tag: "api");

    // Create local repository and API repository instances
    Get.put<TaskRepository>(TaskRepositoryImpl(taskDataSource: localDataSource),
        tag: "local");
    Get.put<TaskRepository>(TaskRepositoryImpl(taskDataSource: apiDataSource),
        tag: "api");
  }
}
