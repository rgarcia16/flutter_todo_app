import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

/// Base controller class providing common functionalities.
class BaseController extends GetxController {
  /// Observable indicating whether loading indicator is displayed.
  final isLoading = false.obs;

  /// Dismisses the loading indicator.
  void closeLoading() => EasyLoading.dismiss();

  /// Displays a loading indicator.
  ///
  /// @param customText: Custom text to display in the loading indicator [String?].
  void showLoading([String? customText]) {
    EasyLoading.show(
      status: customText ?? 'LOADING'.tr,
      maskType: EasyLoadingMaskType.black,
    );
  }

  /// Displays a success message as a snackbar.
  ///
  /// @param title: Title of the success message [String].
  /// @param subtitle: Subtitle of the success message [String].
  void showSuccessMessage(String title, String subtitle) {
    Get.snackbar(
      title,
      subtitle,
      colorText: Colors.white,
      backgroundColor: Colors.green.shade300,
      icon: const Center(
        child: FaIcon(FontAwesomeIcons.check, color: Colors.white),
      ),
    );
  }

  /// Displays an error message as a snackbar.
  ///
  /// @param title: Title of the error message [String].
  /// @param subtitle: Subtitle of the error message [String].
  void showErrorMessage(String title, String subtitle) {
    Get.snackbar(
      title,
      subtitle,
      colorText: Colors.white,
      backgroundColor: Colors.redAccent.shade100,
      icon: const Center(
        child: FaIcon(FontAwesomeIcons.circleExclamation, color: Colors.white),
      ),
    );
  }
}
