import 'base_controller.dart';

/// Controller class for managing main application functionalities.
class MainController extends BaseController {
  @override
  void onInit() {
    super.onInit();
    // Initialization logic can be added here if needed
  }
}
