/// Represents a task in the to-do list.
class Task {
  /// The unique identifier of the task.
  final String id;

  /// The title of the task.
  final String title;

  /// The status of the task. Defaults to 0 if not provided.
  final int? status;

  /// Constructor for the [Task] class.
  ///
  /// @param id: The unique identifier of the task [String].
  /// @param title: The title of the task [String].
  /// @param status: The status of the task. Defaults to 0 if not provided [int].
  Task({
    required this.id,
    required this.title,
    this.status = 0,
  });

  /// Creates a copy of the task with the specified fields updated.
  ///
  /// @param id: The new identifier for the task [String].
  /// @param title: The new title for the task [String].
  /// @param status: The new status for the task [int].
  ///
  /// @returns: A new instance of the [Task] class with the specified fields updated.
  Task copyWith({
    String? id,
    String? title,
    int? status,
  }) {
    return Task(
      id: id ?? this.id,
      title: title ?? this.title,
      status: status ?? this.status,
    );
  }
}
