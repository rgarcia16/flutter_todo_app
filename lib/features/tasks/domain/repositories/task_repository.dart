import 'package:todo_app/features/tasks/domain/entities/task.dart';

/// This abstract class defines the contract for a task repository.
///
/// Implementations of this class will provide methods to interact with
/// persistent storage or remote services to perform CRUD operations on tasks.
abstract class TaskRepository {
  /// Creates a new task.
  ///
  /// @param task: The task to be created [Task].
  ///
  /// @returns: A [Future] that completes once the task has been created.
  Future<void> createTask(Task task);

  /// Updates an existing task.
  ///
  /// @param task: The updated task [Task].
  ///
  /// @returns: A [Future] that completes once the task has been updated.
  Future<void> updateTask(Task task);

  /// Deletes a task by its ID.
  ///
  /// @param id: The ID of the task to be deleted [String].
  ///
  /// @returns: A [Future] that completes once the task has been deleted.
  Future<void> deleteTask(String id);

  /// Retrieves a list of all tasks.
  ///
  /// @returns: A [Future] that completes with a list of tasks [List<Task>].
  Future<List<Task>> listTask();
}
