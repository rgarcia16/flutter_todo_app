import 'package:todo_app/features/tasks/domain/entities/task.dart';

import '../repositories/task_repository.dart';

/// Represents a use case for creating a task.
class CreateTaskUseCases {
  /// The local task repository.
  final TaskRepository localRepository;

  /// The API task repository. For the future use
  final TaskRepository apiRepository;

  /// Constructor for the [CreateTaskUseCases] class.
  ///
  /// @param localRepository: The local task repository [TaskRepository].
  /// @param apiRepository: The API task repository [TaskRepository].
  CreateTaskUseCases({
    required this.localRepository,
    required this.apiRepository,
  });

  /// Executes the use case to create a task.
  ///
  /// @param task: The task to be created [Task].
  ///
  /// @returns: A [Future] that completes once the task has been created.
  Future<void> execute(Task task) {
    return localRepository.createTask(task);
  }
}
