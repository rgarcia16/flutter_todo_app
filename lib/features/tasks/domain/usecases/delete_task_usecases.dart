import '../repositories/task_repository.dart';

/// Represents a use case for deleting a task.
class DeleteTaskUseCases {
  /// The local task repository.
  final TaskRepository localRepository;

  /// The API task repository.
  final TaskRepository apiRepository;

  /// Constructor for the [DeleteTaskUseCases] class.
  ///
  /// @param localRepository: The local task repository [TaskRepository].
  /// @param apiRepository: The API task repository [TaskRepository].
  DeleteTaskUseCases({
    required this.localRepository,
    required this.apiRepository,
  });

  /// Executes the use case to delete a task by its ID.
  ///
  /// @param id: The ID of the task to be deleted [String].
  ///
  /// @returns: A [Future] that completes once the task has been deleted.
  Future<void> execute(String id) async {
    return localRepository.deleteTask(id);
  }
}
