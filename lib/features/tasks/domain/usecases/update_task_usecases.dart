import 'package:todo_app/features/tasks/domain/entities/task.dart';
import 'package:todo_app/features/tasks/domain/repositories/task_repository.dart';

/// Represents a use case for updating a task.
class UpdateTaskUseCases {
  /// The local task repository.
  final TaskRepository localRepository;

  /// The API task repository.
  final TaskRepository apiRepository;

  /// Constructor for the [UpdateTaskUseCases] class.
  ///
  /// @param localRepository: The local task repository [TaskRepository].
  /// @param apiRepository: The API task repository [TaskRepository].
  UpdateTaskUseCases({
    required this.localRepository,
    required this.apiRepository,
  });

  /// Executes the use case to update a task.
  ///
  /// @param task: The updated task [Task].
  ///
  /// @returns: A [Future] that completes once the task has been updated.
  Future<void> execute(Task task) async {
    await localRepository.updateTask(task);
  }
}
