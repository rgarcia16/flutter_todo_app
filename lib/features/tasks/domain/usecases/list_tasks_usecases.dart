import 'package:todo_app/features/tasks/domain/entities/task.dart';
import 'package:todo_app/features/tasks/domain/repositories/task_repository.dart';

/// Represents a use case for listing tasks.
class ListTasksUseCases {
  /// The local task repository.
  final TaskRepository localRepository;

  /// The API task repository.
  final TaskRepository apiRepository;

  /// Constructor for the [ListTasksUseCases] class.
  ///
  /// @param localRepository: The local task repository [TaskRepository].
  /// @param apiRepository: The API task repository [TaskRepository].
  ListTasksUseCases({
    required this.localRepository,
    required this.apiRepository,
  });

  /// Executes the use case to retrieve a list of tasks.
  ///
  /// @returns: A [Future] that completes with a list of tasks [List<Task>].
  Future<List<Task>> execute() async {
    return await localRepository.listTask();
  }
}
