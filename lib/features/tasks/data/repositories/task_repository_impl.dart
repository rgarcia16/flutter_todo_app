import 'package:todo_app/features/tasks/domain/datasources/task_datasource.dart';
import 'package:todo_app/features/tasks/domain/entities/task.dart';
import 'package:todo_app/features/tasks/domain/repositories/task_repository.dart';

/// Implementation of a task repository.
class TaskRepositoryImpl implements TaskRepository {
  /// The data source responsible for handling task operations.
  final TaskDataSource taskDataSource;

  /// Constructor for the [TaskRepositoryImpl] class.
  ///
  /// @param taskDataSource: The data source for task operations [TaskDataSource].
  TaskRepositoryImpl({required this.taskDataSource});

  /// Creates a new task.
  ///
  /// @param task: The task to be created [Task].
  ///
  /// @returns: A [Future] that completes once the task has been created.
  @override
  Future<void> createTask(Task task) async {
    await taskDataSource.createTask(task);
  }

  /// Deletes a task by its ID.
  ///
  /// @param id: The ID of the task to be deleted [String].
  ///
  /// @returns: A [Future] that completes once the task has been deleted.
  @override
  Future<void> deleteTask(String id) async {
    await taskDataSource.deleteTask(id);
  }

  /// Retrieves a list of all tasks.
  ///
  /// @returns: A [Future] that completes with a list of tasks [List<Task>].
  @override
  Future<List<Task>> listTask() async {
    return taskDataSource.listTask();
  }

  /// Updates an existing task.
  ///
  /// @param task: The updated task [Task].
  ///
  /// @returns: A [Future] that completes once the task has been updated.
  @override
  Future<void> updateTask(Task task) async {
    await taskDataSource.updateTask(task);
  }
}
