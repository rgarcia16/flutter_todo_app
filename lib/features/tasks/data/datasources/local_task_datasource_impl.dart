import 'package:todo_app/features/tasks/data/models/task_model.dart';
import 'package:todo_app/features/tasks/domain/datasources/task_datasource.dart';
import 'package:todo_app/features/tasks/domain/entities/task.dart';
import 'package:todo_app/shared/local_datasources/shared_preferences/preferences_config.dart';

/// Implementation of a local task data source using SharedPreferences.
class LocalTaskDataSourceImpl implements TaskDataSource {
  /// Creates a new task in the local data source.
  ///
  /// @param task: The task to be created [Task].
  ///
  /// @returns: A [Future] that completes once the task has been created.
  @override
  Future<void> createTask(Task task) async {
    TaskModel taskModel = TaskModel(
      task: Task(
        id: task.id,
        status: task.status,
        title: task.title,
      ),
    );
    SharedPreferencesService.saveMap(task.id, taskModel.toJson());
  }

  /// Deletes a task from the local data source by its ID.
  ///
  /// @param id: The ID of the task to be deleted [String].
  ///
  /// @returns: A [Future] that completes once the task has been deleted.
  @override
  Future<void> deleteTask(String id) async {
    await SharedPreferencesService.remove(id);
  }

  /// Retrieves a list of tasks from the local data source.
  ///
  /// @returns: A [Future] that completes with a list of tasks [List<Task>].
  @override
  Future<List<Task>> listTask() async {
    final List<Task> tasks = SharedPreferencesService.getAllData()
        .map((task) => TaskModel.fromMap(task).task)
        .toList();
    return tasks;
  }

  /// Updates a task in the local data source.
  ///
  /// @param task: The updated task [Task].
  ///
  /// @returns: A [Future] that completes once the task has been updated.
  @override
  Future<void> updateTask(Task task) async {
    TaskModel taskModel = TaskModel(
      task: Task(
        id: task.id,
        status: task.status,
        title: task.title,
      ),
    );
    SharedPreferencesService.updateData(task.id, taskModel.toJson());
  }
}
