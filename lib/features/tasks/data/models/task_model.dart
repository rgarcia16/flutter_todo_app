import '../../domain/entities/task.dart';

/// Represents a model for a task entity.
class TaskModel {
  /// The task entity.
  final Task task;

  /// Constructor for the [TaskModel] class.
  ///
  /// @param task: The task entity [Task].
  TaskModel({required this.task});

  /// Constructs a [TaskModel] instance from a JSON map.
  ///
  /// @param json: The JSON map containing task data [Map<String, dynamic>].
  ///
  /// @returns: A [TaskModel] instance constructed from the JSON map.
  factory TaskModel.fromJson(Map<String, dynamic> json) {
    final entityTask = Task(
      id: json["id"],
      title: json["title"],
      status: json["status"],
    );
    return TaskModel(task: entityTask);
  }

  /// Constructs a [TaskModel] instance from a map.
  ///
  /// @param map: The map containing task data [Map<String, dynamic>].
  ///
  /// @returns: A [TaskModel] instance constructed from the map.
  factory TaskModel.fromMap(Map<String, dynamic> map) {
    final entityTask = Task(
      id: map['id'] as String,
      title: map['title'] as String,
      status: map['status'] as int,
    );
    return TaskModel(task: entityTask);
  }

  /// Converts the [TaskModel] instance to a JSON map.
  ///
  /// @returns: A JSON map representation of the task [Map<String, dynamic>].
  Map<String, dynamic> toJson() => {
        "id": task.id,
        "title": task.title,
        "status": task.status,
      };
}
