import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo_app/config/routes/routes.dart';
import 'package:todo_app/config/theme/app_theme.dart';

class OnboardingPage extends StatelessWidget {
  const OnboardingPage({super.key});

  @override
  Widget build(BuildContext context) {
    const welcomeText =
        "Get started with Todo App now! Manage your daily tasks easily and efficiently."
        " From making the grocery list to planning important projects,"
        " Todo App helps you keep everything under control. Click 'Get Started' and start simplifying your life today!";

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/logo.jpg',
              width: 200,
              height: 200,
            ),
            const Text(
              'To-Do App',
              style: TextStyle(color: Colors.deepPurple, fontSize: 24),
            ),
            heightSpace20,
            const Text(
              welcomeText,
              textAlign: TextAlign.center,
            ),
            heightSpace20,
            ElevatedButton(
              onPressed: () {
                Get.offNamed(RoutesPath.listTask);
              },
              child: const Text('¡Get Started!'),
            ),
          ],
        ),
      ),
    );
  }
}
