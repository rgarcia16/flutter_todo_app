import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo_app/config/routes/routes.dart';
import 'package:todo_app/features/tasks/presentation/list_tasks/widgets/list_tasks_widget.dart';

class ListTasksPage extends StatelessWidget {
  const ListTasksPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'List Tasks',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: Colors.deepPurple,
        ),
        body: const ListTasksWidget(),
        floatingActionButton: const _FloatingButtonWidget());
  }
}

class _FloatingButtonWidget extends StatelessWidget {
  const _FloatingButtonWidget();

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: const Icon(
        Icons.add,
      ),
      onPressed: () {
        Get.toNamed(RoutesPath.createTask);
      },
    );
  }
}
