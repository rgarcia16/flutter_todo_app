import 'package:get/get.dart';
import 'package:todo_app/features/tasks/domain/repositories/task_repository.dart';
import 'package:todo_app/features/tasks/domain/usecases/delete_task_usecases.dart';
import 'package:todo_app/features/tasks/domain/usecases/list_tasks_usecases.dart';
import 'package:todo_app/features/tasks/domain/usecases/update_task_usecases.dart';
import 'package:todo_app/features/tasks/presentation/list_tasks/controllers/list_tasks_controller.dart';

/// Binding class for listing tasks.
class ListTasksBinding extends Bindings {
  /// Initializes dependencies for listing tasks.
  ///
  /// @dependencies:
  ///   - [DeleteTaskUseCases] instance with local and API repositories.
  ///   - [UpdateTaskUseCases] instance with local and API repositories.
  ///   - [ListTasksUseCases] instance with local and API repositories.
  ///   - [ListTasksController] instance.
  @override
  void dependencies() {
    Get.put(
      DeleteTaskUseCases(
        localRepository: Get.find<TaskRepository>(tag: "local"),
        apiRepository: Get.find<TaskRepository>(tag: "api"),
      ),
    );
    Get.put(
      UpdateTaskUseCases(
        localRepository: Get.find<TaskRepository>(tag: "local"),
        apiRepository: Get.find<TaskRepository>(tag: "api"),
      ),
    );
    Get.put(
      ListTasksUseCases(
        localRepository: Get.find<TaskRepository>(tag: "local"),
        apiRepository: Get.find<TaskRepository>(tag: "api"),
      ),
    );
    Get.put(ListTasksController());
  }
}
