import 'package:get/get.dart';
import 'package:todo_app/features/common/presentation/controllers/base_controller.dart';
import 'package:todo_app/features/tasks/domain/entities/task.dart';
import 'package:todo_app/features/tasks/domain/usecases/delete_task_usecases.dart';
import 'package:todo_app/features/tasks/domain/usecases/list_tasks_usecases.dart';
import 'package:todo_app/features/tasks/domain/usecases/update_task_usecases.dart';

/// Controller responsible for managing the list of tasks.
class ListTasksController extends BaseController {
  /// Use case for listing tasks.
  final listTasksUseCases = Get.find<ListTasksUseCases>();

  /// Use case for updating task status.
  final updateStatusTaskUseCases = Get.find<UpdateTaskUseCases>();

  /// Use case for deleting tasks.
  final deleteTaskUseCases = Get.find<DeleteTaskUseCases>();

  /// RxList to hold the list of tasks.
  RxList<Task> tasks = <Task>[].obs;

  /// Indicates whether the initial data loading is in progress.
  bool initialLoading = true;

  @override
  void onInit() {
    super.onInit();
    listsTask();
  }

  /// Updates the status of a task.
  ///
  /// @param index: The index of the task in the list [int].
  /// @param status: The new status of the task [int].
  ///
  /// @returns: A [Future] that completes once the task status is updated.
  Future<void> updateStatusTask(int index, int status) async {
    final Task task = tasks[index].copyWith(status: status);
    await updateStatusTaskUseCases.execute(task);
    listsTask();
  }

  /// Deletes a task.
  ///
  /// @param index: The index of the task in the list [int].
  ///
  /// @returns: A [Future] that completes once the task is deleted.
  Future<void> deleteTask(int index) async {
    final Task task = tasks[index].copyWith();
    await deleteTaskUseCases.execute(task.id);
    listsTask();
  }

  /// Retrieves the list of tasks.
  ///
  /// @returns: A [Future] that completes once the list of tasks is retrieved.
  Future<void> listsTask() async {
    final listTask = await listTasksUseCases.execute();
    tasks.value = listTask;
  }
}
