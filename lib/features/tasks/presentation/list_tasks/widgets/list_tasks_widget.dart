import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo_app/config/routes/routes.dart';
import 'package:todo_app/features/tasks/domain/entities/task.dart';
import 'package:todo_app/features/tasks/presentation/list_tasks/controllers/list_tasks_controller.dart';

class ListTasksWidget extends GetView<ListTasksController> {
  const ListTasksWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ListView.builder(
          padding: const EdgeInsets.only(top: 15),
          itemCount: controller.tasks.length,
          itemBuilder: (context, index) {
            final Task task = controller.tasks[index];
            return ListTile(
              title: InkWell(
                onTap: () => Get.toNamed(
                  RoutesPath.createTask,
                  arguments: {'task': task},
                ),
                child: Text(
                  task.title,
                  style: TextStyle(
                    decoration:
                        task.status == 1 ? TextDecoration.lineThrough : null,
                  ),
                ),
              ),
              leading: Checkbox(
                value: task.status == 1,
                onChanged: (bool? isChecked) {
                  final int newStatus = isChecked! ? 1 : 0;
                  controller.updateStatusTask(index, newStatus);
                },
              ),
              trailing: IconButton(
                icon: const Icon(Icons.delete),
                onPressed: () {
                  controller.deleteTask(index);
                },
              ),
            );
          }),
    );
  }
}
