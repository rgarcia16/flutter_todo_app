import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo_app/features/tasks/presentation/create_update_task/widgets/create_update_task_form_widget.dart';

class CreateUpdateTaskPage extends StatelessWidget {
  const CreateUpdateTaskPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Create Task',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.deepPurple,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new_rounded),
          color: Colors.white,
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: const CreateTaskFormWidget(),
    );
  }
}
