import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo_app/config/theme/app_theme.dart';
import 'package:todo_app/features/tasks/presentation/create_update_task/controllers/create_update_task_controller.dart';

class CreateTaskFormWidget extends GetView<CreateUpdateTaskController> {
  const CreateTaskFormWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Form(
      key: controller.form,
      child: ListView(
        padding: const EdgeInsets.all(30),
        children: [
          TextFormField(
            decoration: const InputDecoration(
                labelText: 'Title', hintText: 'Ex: Learn Flutter'),
            validator: (title) => controller.validateTitle(title),
            controller: controller.title,
          ),
          heightSpace24,
          ElevatedButton(
            onPressed: () => controller.createUpdateTask(),
            child: const Text('Save'),
          ),
        ],
      ),
    );
  }
}
