import 'package:get/get.dart';
import 'package:todo_app/features/tasks/domain/repositories/task_repository.dart';
import 'package:todo_app/features/tasks/domain/usecases/create_task_usecases.dart';
import 'package:todo_app/features/tasks/presentation/create_update_task/controllers/create_update_task_controller.dart';

/// Binding class for creating and updating tasks.
class CreateUpdateTaskBinding extends Bindings {
  /// Initializes dependencies for creating and updating tasks.
  ///
  /// @dependencies:
  ///   - [CreateTaskUseCases] instance with local and API repositories.
  ///   - [CreateUpdateTaskController] instance.
  @override
  void dependencies() {
    Get.put(
      CreateTaskUseCases(
        localRepository: Get.find<TaskRepository>(tag: "local"),
        apiRepository: Get.find<TaskRepository>(tag: "api"),
      ),
    );
    Get.put(CreateUpdateTaskController());
  }
}
