import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:todo_app/features/common/presentation/controllers/base_controller.dart';
import 'package:todo_app/features/tasks/domain/entities/task.dart';
import 'package:todo_app/features/tasks/domain/usecases/create_task_usecases.dart';
import 'package:todo_app/features/tasks/domain/usecases/update_task_usecases.dart';
import 'package:todo_app/features/tasks/presentation/list_tasks/controllers/list_tasks_controller.dart';
import 'package:uuid/uuid.dart';

/// Controller responsible for managing task creation and updating.
class CreateUpdateTaskController extends BaseController {
  /// Controller for managing the list of tasks.
  final listController = Get.find<ListTasksController>();

  /// Use case for creating tasks.
  final createTaskUseCases = Get.find<CreateTaskUseCases>();

  /// Use case for updating task status.
  final updateStatusTaskUseCases = Get.find<UpdateTaskUseCases>();

  /// Text controller for the task title.
  final title = TextEditingController();

  /// Text controller for the task description.
  final description = TextEditingController();

  /// Text controller for the task status.
  final status = TextEditingController();

  /// Form key for validation.
  final form = GlobalKey<FormState>();

  /// Rxn holding the task parameters.
  final taskParams = Rxn<Task>();

  @override
  void onInit() {
    super.onInit();
    taskParams.value = Get.arguments?['task'];
    title.text = taskParams.value?.title ?? '';
  }

  /// Validates the title field.
  ///
  /// @param title: The title to validate [String?].
  ///
  /// @returns: An error message if title is null or blank, otherwise null.
  String? validateTitle(String? title) {
    final bool isNullOrBlank = GetUtils.isNullOrBlank(title) ?? false;
    if (isNullOrBlank) {
      return 'Title is required';
    }
    return null;
  }

  /// Creates or updates a task based on form validation.
  Future<void> createUpdateTask() async {
    if (form.currentState!.validate()) {
      if (taskParams.value == null) {
        _createTask();
      } else {
        _updateTask(taskParams.value!.copyWith(title: title.text));
      }
      return;
    }
    showErrorMessage("Attention", "Invalid form");
  }

  /// Creates a new task.
  Future<void> _createTask() async {
    const uuid = Uuid();
    String id = uuid.v4();
    final task = Task(id: id, title: title.text);
    await createTaskUseCases.execute(task);
    listController.listsTask();
    Get.back();
    showSuccessMessage("Task created", "Task created successfully");
    return;
  }

  /// Updates an existing task.
  ///
  /// @param task: The task to update [Task].
  ///
  /// @returns: A [Future] that completes once the task has been updated.
  Future<void> _updateTask(Task task) async {
    await updateStatusTaskUseCases.execute(task);
    listController.listsTask();
    Get.back();
    showSuccessMessage("Task updated", "Your task ${task.id} has been updated");
    return;
  }
}
