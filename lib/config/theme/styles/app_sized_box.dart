part of app_theme;

const double defaultPaddingSize = 16;

const SizedBox heightSpace2 = SizedBox(height: 2);
const SizedBox heightSpace4 = SizedBox(height: 4);
const SizedBox heightSpace5 = SizedBox(height: 5);
const SizedBox heightSpace8 = SizedBox(height: 8);

const SizedBox heightSpace10 = SizedBox(height: 10.0);
const SizedBox heightSpace12 = SizedBox(height: 12);
const SizedBox heightSpace15 = SizedBox(height: 15);
const SizedBox heightSpace16 = SizedBox(height: 16);
const SizedBox heightSpace20 = SizedBox(height: 20);

const SizedBox heightSpace24 = SizedBox(height: 24);
const SizedBox heightSpace25 = SizedBox(height: 25);
const SizedBox heightSpace30 = SizedBox(height: 30);
const SizedBox heightSpace32 = SizedBox(height: 32);
const SizedBox heightSpace40 = SizedBox(height: 40);
const SizedBox heightSpace50 = SizedBox(height: 50);
const SizedBox heightSpace80 = SizedBox(height: 80);
const SizedBox heightSpace100 = SizedBox(height: 100);
const SizedBox heightSpace200 = SizedBox(height: 200);

const SizedBox widthSpace2 = SizedBox(width: 2);
const SizedBox widthSpace5 = SizedBox(width: 5);
const SizedBox widthSpace10 = SizedBox(width: 10.0);
const SizedBox widthSpace15 = SizedBox(width: 15);
const SizedBox widthSpace20 = SizedBox(width: 20);
const SizedBox widthSpace30 = SizedBox(width: 30);
