library app_theme;

export 'styles/app_images.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

part 'styles/app_colors.dart';
part 'styles/app_sized_box.dart';
part 'styles/app_text_style.dart';

class AppTheme {
  static AppStyle get style => AppStyle();
  static AppColors get colors => Get.isDarkMode ? DarkColors() : LightColors();
}
