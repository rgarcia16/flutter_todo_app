import 'package:get/get.dart';
import 'package:todo_app/features/tasks/presentation/create_update_task/controllers/create_update_task_binding.dart';
import 'package:todo_app/features/tasks/presentation/create_update_task/pages/create_update_task_page.dart';
import 'package:todo_app/features/tasks/presentation/list_tasks/controllers/list_tasks_binding.dart';
import 'package:todo_app/features/tasks/presentation/list_tasks/pages/list_tasks_page.dart';
import 'package:todo_app/features/tasks/presentation/onboarding/pages/onboarding_page.dart';

class RoutesPath {
  static String listTask = '/listTaskPage';
  static String editTask = '/editTaskPage';
  static String createTask = '/createTaskPage';
  static String deleteTask = '/deleteTaskPage';
  static String changeStatusTask = 'changeStatusTaskPage';
  static String onboarding = '/onboardingPage';

  static final List<GetPage> pages = [
    GetPage(
      name: RoutesPath.onboarding,
      page: () => const OnboardingPage(),
    ),
    GetPage(
      name: RoutesPath.createTask,
      page: () => const CreateUpdateTaskPage(),
      binding: CreateUpdateTaskBinding(),
    ),
    GetPage(
      name: RoutesPath.listTask,
      page: () => const ListTasksPage(),
      binding: ListTasksBinding(),
    ),
  ];
}
