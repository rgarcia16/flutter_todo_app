import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

/// Service class for interacting with SharedPreferences.
class SharedPreferencesService {
  /// Instance of SharedPreferences.
  static late SharedPreferences _preferences;

  /// Initializes the SharedPreferences instance.
  ///
  /// @returns: A [Future] that completes once initialization is done.
  static Future<void> init() async {
    _preferences = await SharedPreferences.getInstance();
    //_preferences.clear();
  }

  /// Retrieves the SharedPreferences instance.
  ///
  /// @returns: The instance of SharedPreferences.
  static SharedPreferences get instance => _preferences;

  /// Saves a map to SharedPreferences with the given key.
  ///
  /// @param key: The key to identify the data [String].
  /// @param map: The map to save [Map<String, dynamic>].
  ///
  /// @returns: A [Future] that completes once the data is saved.
  static Future<void> saveMap(String key, Map<String, dynamic> map) async {
    String jsonString = json.encode(map);
    await _preferences.setString(key, jsonString);
  }

  /// Retrieves a map from SharedPreferences using the given key.
  ///
  /// @param key: The key to identify the data [String].
  ///
  /// @returns: The retrieved map, or null if not found [Map<String, dynamic>?].
  static Map<String, dynamic>? getMap(String key) {
    String? jsonString = _preferences.getString(key);
    if (jsonString != null) {
      return json.decode(jsonString);
    } else {
      return null;
    }
  }

  /// Retrieves all data from SharedPreferences as a list of maps.
  ///
  /// @returns: A list containing all the data as maps [List<Map<String, dynamic>>].
  static List<Map<String, dynamic>> getAllData() {
    Set<String> keys = _preferences.getKeys();
    List<Map<String, dynamic>> allData = [];

    keys.forEach((key) {
      Map<String, dynamic>? taskData = _preferences.getString(key) != null
          ? json.decode(_preferences.getString(key)!)
          : null;
      if (taskData != null) {
        allData.add(taskData);
      }
    });

    return allData;
  }

  /// Updates data in SharedPreferences with the given key.
  ///
  /// @param key: The key to identify the data [String].
  /// @param value: The new value to update [dynamic].
  ///
  /// @returns: A [Future] that completes once the data is updated.
  static Future<void> updateData(String key, dynamic value) async {
    await saveMap(key, value);
  }

  /// Removes data from SharedPreferences using the given key.
  ///
  /// @param key: The key to identify the data [String].
  ///
  /// @returns: A [Future] that completes once the data is removed.
  static Future<void> remove(String key) async {
    await _preferences.remove(key);
  }
}
