import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:todo_app/config/routes/routes.dart';
import 'package:todo_app/features/common/presentation/controllers/main_binding.dart';
import 'package:todo_app/shared/local_datasources/shared_preferences/preferences_config.dart';

void main() async {
  runApp(const TodoApp());
  await SharedPreferencesService.init();
}

class TodoApp extends StatelessWidget {
  const TodoApp({super.key});
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'To-Do App',
      theme: ThemeData(useMaterial3: true, colorSchemeSeed: Colors.deepPurple),
      debugShowCheckedModeBanner: false,
      builder: EasyLoading.init(),
      initialBinding: MainBinding(),
      initialRoute: RoutesPath.onboarding,
      getPages: RoutesPath.pages,
      locale: Get.deviceLocale,
      fallbackLocale: const Locale('es'),
    );
  }
}
